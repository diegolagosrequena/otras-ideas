<?php

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

define('FS_METHOD', 'direct');

?>
