$(document).ready(function(){

  window.addEventListener("scroll", function(event) {
    var top = this.scrollY;
    if (top>100) {
      $('.navbar-home').addClass('active');
    }
    else {
      $('.navbar-home').removeClass('active');
    }
  }, false);

  // slider

  $('.slider-testimonio').slick({
    dots: true,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    draggable: true,
    responsive: [
     {
       breakpoint: 768,
       settings: {
         dots: true,
         arrows: true,
         slidesToShow: 1,
         draggable: true
       }
     },
     {
       breakpoint: 480,
       settings: {
         dots: true,
         arrows: false,
         slidesToShow: 1,
         draggable: true,
         centerMode: false,
       }
     }
   ]
  });

  $('.slider-patners').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    draggable: true,
    responsive: [
     {
       breakpoint: 768,
       settings: {
         dots: true,
         arrows: true,
         slidesToShow: 1,
         draggable: true
       }
     },
     {
       breakpoint: 480,
       settings: {
         dots: true,
         arrows: false,
         slidesToShow: 1,
         draggable: true
       }
     }
   ]
  });



});
