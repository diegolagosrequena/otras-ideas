<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>

    <meta name="description" content="">
    <!-- <meta name="keywords" content="HTML,HTML5,CSS,CSS3,JavaScript,UX&UI,Wordpress,Diseño Web, Diseño Gráfico"> -->
    <meta name="author" content="Diego Lagos Requena">
    <meta property="og:url" content="www.diegolagosr.cl">
    <meta property="og:title" content="Otras Ideas - Agencia Marketing Digital">
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/share.jpg">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Otras Ideas - Agencia Marketing Digital">
    <meta property="og:description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/slick.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/slick-theme.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/scrolling-nav.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css" rel="stylesheet">


    <?php wp_head(); ?>
</head>
<body>

  <nav class="navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand" href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" alt="Otras Ideas"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php wp_nav_menu(
  				array (
					'items_wrap'      => '<ul class="navbar-nav">%3$s</ul>',
					// 'theme_location' => 'ventto-nav'
					)
  			);?>
        <ul class="rrss">
          <li><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
          <li><a href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
          <!-- <li><a href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a></li> -->
        </ul>
      </div>
    </div>
  </nav>