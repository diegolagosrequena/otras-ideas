<?php get_header(); ?>

  <header style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>);">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center">
        <div class="col-lg-6 text-center">
          <h1><?php the_field('titulo_header'); ?></h1>
          <p><?php the_field('bajada_header'); ?></p>
        </div>
      </div>
    </div>
  </header>

  <section id="servicios">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <?php if( have_rows('servicios') ): ?>
            <h2><?php echo get_field_object('servicios')['label']; ?></h2>


            <div class="row">
              <?php while( have_rows('servicios') ): the_row(); ?>
                <div class="col-lg-4">
                  <div class="content-servicios">
                    <div class="row">
                      <div class="col-lg-3">
                        <img src="<?php the_sub_field('icono_servicio'); ?>" alt="" >
                      </div>
                      <div class="col-lg-9">
                        <h3><?php the_sub_field('titulo_servicios'); ?></h3>
                        <p><?php the_sub_field('descripcion_servicio'); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endwhile; ?>

              <?php while( have_rows('brouchure_digital') ): the_row(); ?>
                <div class="col-lg-4">
                  <div class="content-servicios content-brouchure" onclick="window.open('<?php the_sub_field('link_brouchure');?>','_blank')">
                    <div class="row">
                      <div class="col-lg-3">
                        <img src="<?php the_sub_field('icono_brouchure'); ?>" alt="" >
                      </div>
                      <div class="col-lg-9">
                        <h3><?php the_sub_field('titulo_brouchures'); ?></h3>
                        <p><?php the_sub_field('descripcion_brouchure'); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endwhile; ?>
            
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>

  <section id="clientes">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <?php if( have_rows('clientes') ): ?>
            
          <div class="col-lg-12">
            <h2><?php echo get_field_object('clientes')['label']; ?></h2>
          </div>
          
          <?php while( have_rows('clientes') ): the_row(); ?>
            <div class="col-lg-2 col-sm-2 col-xs-6 col-6">
              <img src="<?php the_sub_field('logo_cliente'); ?>" alt="">
            </div>
          <?php endwhile; ?>

        <?php endif; ?>
      </div>
    </div>
  </section>

  <section id="equipo">
    <div class="container">

      <div class="row">
        <div class="col-lg-3">
        <h2><?php echo get_field_object('nuestro_equipo')['label']; ?></h2>
        </div>
        <div class="col-lg-8">
          <div class="info-equipo">
            <p><?php the_field('nuestro_equipo'); ?></p>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="row">

            <?php if( have_rows('equipo') ): ?>
              <?php while( have_rows('equipo') ): the_row(); ?>
                <div class="col-lg-6">
                  <div class="row mb-3">

                    <div class="col-lg-6 p-0">
                      <img src="<?php the_sub_field('imagen'); ?>" alt="" class="w-100">
                    </div>
                    
                    <div class="col-lg-6 p-0">
                      <div class="content-equipo">
                        <h4><?php the_sub_field('nombre'); ?></h4>
                        <h5><?php the_sub_field('cargo'); ?></h5>
                        <p><?php the_sub_field('descripcion'); ?></p>
                        <p><?php the_sub_field('resena'); ?></p>

                      </div>
                    </div>

                  </div>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>

          </div>
        </div>
      </div>

    </div>
  </section>

  <section id="testimonio">
    <div class="container">
    <h2><?php echo get_field_object('testimonios')['label']; ?></h2>
      <div class="row justify-content-center">
        <div class="col-lg-10">
          <div class="slider-testimonio">
            <?php if( have_rows('testimonios') ): ?>
              <?php while( have_rows('testimonios') ): the_row(); ?>
                <div>
                  <p><?php the_sub_field('texto_testimonio'); ?></p>
                  <p><strong><?php the_sub_field('cliente'); ?></strong></p>
                  <p><span><?php the_sub_field('cargo'); ?></span></p>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="contacto">
    <div class="bg-contacto" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-contact.jpg);"></div>
    <div class="container">
      <div class="row  h-100 align-items-center justify-content-center">
        <div class="col-lg-8">
          <div class="container-contacto">
            <div class="form">
              <h2>Conversemos</h2>
              <?php echo do_shortcode ('[contact-form-7 title="Conversemos"]');?>
            </div>
            <div class="info-form">
              <ul class="contact-info ml-auto">
                <li><i class="fas fa-phone"></i> <?php the_field('contact_telefono'); ?></li>
                <li><i class="fas fa-envelope"></i> <?php the_field('contact_mail'); ?></li>
              </ul>
              <ul class="contact-rrss">
                <li><a href="<?php the_field('url_facebook'); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="<?php the_field('url_twitter'); ?>" target="_blank"><i class="fab fa-twitter"></i> </a></li>
                <!-- <li><a href=""><i class="fab fa-instagram"></i></a></li> -->
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
