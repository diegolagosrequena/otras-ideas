  <footer>
    <div class="container">
      <div class="row h-100 align-items-center justify-content-center">
        <div class="col-lg-8">
        <?php if( have_rows('patners') ): ?>
          <h2><?php echo get_field_object('patners')['label']; ?></h2>
          <div class="slider-patners">

              <?php while( have_rows('patners') ): the_row(); ?>
                <div>
                  <a href="<?php the_sub_field('url-patners'); ?>" target="_blank">
                    <img src="<?php the_sub_field('logo-patners'); ?>" alt="">
                  </a>
                </div>
              <?php endwhile; ?>
            
          </div>
        <?php endif; ?>
      </div>
    </div>
  </footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/slick.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.fancybox.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.easing.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>

  <?php wp_footer(); ?>
</body>
</html>
